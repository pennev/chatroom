<?php

namespace App\Services;

class NotesService extends BaseService
{

    public function getOne($id)
    {
        return $this->db->fetchAssoc("SELECT * FROM notes WHERE id=?", [(int) $id]);
    }

    public function getAll()
    {
        return $this->db->fetchAll("SELECT * FROM users");
    }

//    public function singIn($username, $password)
//    {
//        return $this->db->fetchAssoc("SELECT * FROM users");
//    }

    function save($note)
    {
        //var_dump($note);exit;
        $this->db->insert("users", $note);
        return $this->db->lastInsertId();
    }

    function update($id, $note)
    {
        return $this->db->update('notes', $note, ['id' => $id]);
    }

    function delete($id)
    {
        return $this->db->delete("notes", array("id" => $id));
    }

    public function testGet()
    {
        return $this->db->fetchAssoc("SELECT * FROM users");
    }


}
